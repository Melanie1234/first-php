<?php

namespace App;

class Person {
    public string $name;
    public string $firstName;

    /**
     * @param string $name
     * @param string $firstName
     */
    public function __construct(string $name, string $firstName) {
    	$this->name = $name;
    	$this->firstName = $firstName;
    }

    public function introduction():void {
        echo "Hello my name is {$this->firstName} {$this->name}";
    }

    public function greets(Person $target): void {
        echo "Hello {$target->firstName} how are you ?";
        
    } 
}
