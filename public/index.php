
<?php

$nomVarible = 'initial';
$tab = [1, 2, 3, 4];

array_push($tab, 6); //equivalent de tab.push(5) en js
$tab[] = 5; //equivalent de tab.push(5) en js

echo $tab[0];

if(isset($nomVarible)){
    echo $nomVarible;
} else{
    echo "pas de varible";
}

function add(int $a, int $b):int{
    return $a + $b;
}

echo add(5, 6);